export default {
  'dockerps': data => {
    let clean = data.replace(/\n/g, '  ').split('  ').filter(String).map(val => val.trim())
    let column = clean.splice(0, 7)

    let retornable = []
    let item = {}
    let n = 0
    clean.forEach(element => {
      if (n === 5 && element.indexOf('tcp') === -1) {
        item[column[n]] = '-'
        n++
      }
      item[column[n]] = element
      n++

      if (n === 7) {
        retornable.push(item)
        item = {}
        n = 0
      }
    })
    return retornable
  },
  'sites': data => {
    let clean = data.replace(/\n|\t/g, '').split('server ').filter(String)

    let retornable = []
    let item = {}
    clean.forEach(element => {
      element = element.replace(/{listen 80;server_name /, '')
      item.name = element.substr(0, element.indexOf(';'))

      if (element.indexOf('location') !== -1) {
        let locations = element.split('location ')
        item.paths = []

        locations.forEach((location, index) => {
          if (index === 0) return 
          item.paths.push(location.substr(0, location.indexOf(' {')))
        })
      }
      retornable.push(item)
      item = {}
    })
    return retornable
  }
}
